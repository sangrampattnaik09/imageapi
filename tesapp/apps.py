from django.apps import AppConfig


class TesappConfig(AppConfig):
    name = 'tesapp'
