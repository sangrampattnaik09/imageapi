from rest_framework import generics,views,parsers
from .models import *


class ImageAPIView(generics.ListCreateAPIView):
    parser_classes = [parsers.FormParser,parsers.MultiPartParser]
    serializer_class = ImageSerializer
    queryset = Image.objects.all()