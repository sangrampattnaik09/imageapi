from django.db import models
from django.db.models import fields
from django.db.models.fields.files import ImageField
from rest_framework import serializers

# Create your models here.
class Image(models.Model):
    image = models.ImageField(upload_to='images')



class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image']